# README #

Šis ir projekts, kurš tika viedots, notestētu sākotnējās darbības ar Java Persistence API.


### Vides konfigurācija ###

* IntelliJ 14.0.3 Ultimate
* Maven 3.2.5 (nevajag pagaidām lietot 3.3.1 jeb pēdējo versiju)
* JDK 1.8.0_31
* M2_HOME, JAVA_HOME vides mainīgie uzstādīti. %M2_HOME%/bin pievienots PATH (pārbaudīt var vienkārši command promt izsaucot komandu mvn --version)
 